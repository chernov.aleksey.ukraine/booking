import flight.CollectionFlightDao;
import flight.Flight;
import flight.FlightController;
import flight.FlightService;
import menu.Menu;
import reservation.CollectionReservationDao;
import reservation.Reservation;
import reservation.ReservationController;
import reservation.ReservationService;
import java.io.IOException;
import java.util.ArrayList;
public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList <Flight> flights = new ArrayList<>();
        CollectionFlightDao collectionFlightDao = new CollectionFlightDao(flights);
        FlightService flightService = new FlightService(collectionFlightDao);
        FlightController flightController = new FlightController(flightService);
        flightController.downloadData("flight_data");
        ArrayList <Reservation> reservations = new ArrayList<>();
        CollectionReservationDao collectionReservationDao = new CollectionReservationDao(reservations);
        ReservationService reservationService = new ReservationService(collectionReservationDao);
        ReservationController reservationController = new ReservationController(reservationService);
        reservationController.downloadData("reservation_data");
        Menu.menu( flightController, reservationController);
    }
}

