package test;
import flight.CollectionFlightDao;
import flight.Flight;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reservation.CollectionReservationDao;
import reservation.Reservation;
import reservation.ReservationService;

import java.io.File;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class CollectionReservationDaoTest {
    @Mock
    private ArrayList<Reservation> reservationList;
    @InjectMocks
    private CollectionReservationDao collectionReservationDao;
    Reservation reservation1 = new Reservation(1,"0610-JS", new ArrayList<>(List.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7")));
    Reservation reservation2 = new Reservation(2,"0506-NY", new ArrayList<>(List.of("Name3 Surname3", "Name4 Surname4")));
    @Test
    public void testGetAllReservations() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1, reservation2)));
        List<Reservation> actualReservations = collectionReservationDao1.getAllReservations();
        assertEquals(2, actualReservations.size());}
    @Test
    public void testLoadDownloadData() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1, reservation2)));
        List<Reservation> control = collectionReservationDao1.getAllReservations();
        File file = new File("test_reservation_data");
        collectionReservationDao1.loadData((ArrayList<Reservation>) control, "test_reservation_data");
        collectionReservationDao1.setReservationList(new ArrayList<>());
        collectionReservationDao1.downloadData("test_reservation_data");
        file.delete();
        assertEquals(control, collectionReservationDao1.getAllReservations() );}

    @Test
    public void testAddReservation() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1)));
        collectionReservationDao1.addReservation(reservation2);
        assertEquals(2, collectionReservationDao1.getAllReservations().size());}
    @Test
    public void testDeleteReservation() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1, reservation2)));
        collectionReservationDao1.deleteReservation(1);
        assertEquals(1, collectionReservationDao1.getAllReservations().size());}

}
