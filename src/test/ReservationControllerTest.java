package test;
import flight.FlightController;
import flight.FlightService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reservation.CollectionReservationDao;
import reservation.Reservation;
import reservation.ReservationController;
import reservation.ReservationService;

import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class ReservationControllerTest {
    @Mock
    private ReservationService reservationService;
    @InjectMocks
    private ReservationController reservationController;
    Reservation reservation1 = new Reservation(1,"0610-JS", new ArrayList<>(List.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7")));
    Reservation reservation2 = new Reservation(2,"0506-NY", new ArrayList<>(List.of("Name3 Surname3", "Name4 Surname4")));
    @Test
    public void testGetAllReservations() {
        when(reservationService.getAllReservations()).thenReturn(new ArrayList<>(List.of(new Reservation(1,"0610-JS", new ArrayList<>(List.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7"))))));
        List<Reservation> actualReservations = reservationController.getAllReservations();
        assertEquals(1, actualReservations.size());}
    @Test
    public void testReservationSearch() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1)));
        ReservationService reservationService1 = new ReservationService(collectionReservationDao1);
        ReservationController reservationController1 = new ReservationController(reservationService1);
        List<Reservation> expectedReservations = new ArrayList<Reservation>(List.of(reservation1));
        List<Reservation> actualReservations = reservationController1.reservationSearch("Name1 Surname1");
        assertEquals(expectedReservations, actualReservations);}

    @Test
    public void testAddReservation() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1)));
        ReservationService reservationService1 = new ReservationService(collectionReservationDao1);
        ReservationController reservationController1 = new ReservationController(reservationService1);
        reservationController1.addReservation(reservation2);
        assertEquals(2, reservationController1.getAllReservations().size());}
    @Test
    public void testDeleteReservation() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1, reservation2)));
        ReservationService reservationService1 = new ReservationService(collectionReservationDao1);
        ReservationController reservationController1 = new ReservationController(reservationService1);
        reservationController1.deleteReservation(1);
        assertEquals(1, reservationController1.getAllReservations().size());}
    @Test
    public void testFindReservation() {
        when(reservationService.getAllReservations()).thenReturn(new ArrayList<>(List.of(
                new Reservation(1,"0610-JS", new ArrayList<>(List.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7"))),
                new Reservation(2,"0506-NY", new ArrayList<>(List.of("Name3 Surname3", "Name4 Surname4")))
        )));
        Reservation actualReservation = reservationController.findReservation(2);
        assertEquals(reservation2, actualReservation);}
}
