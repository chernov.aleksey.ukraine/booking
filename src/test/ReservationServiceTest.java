package test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reservation.CollectionReservationDao;
import reservation.Reservation;
import reservation.ReservationService;

import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class ReservationServiceTest {
    @Mock
    private CollectionReservationDao collectionReservationDao;
    @InjectMocks
    private ReservationService reservationService;
    Reservation reservation1 = new Reservation(1,"0610-JS", new ArrayList<>(List.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7")));
    Reservation reservation2 = new Reservation(2,"0506-NY", new ArrayList<>(List.of("Name3 Surname3", "Name4 Surname4")));
    @Test
    public void testGetAllReservations() {
        when(collectionReservationDao.getAllReservations()).thenReturn(new ArrayList<>(List.of(new Reservation(1,"0610-JS", new ArrayList<>(List.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7"))))));
        List<Reservation> actualReservations = reservationService.getAllReservations();
        assertEquals(1, actualReservations.size());}
    @Test
    public void testReservationSearch() {
        when(collectionReservationDao.getAllReservations()).thenReturn(new ArrayList<>(List.of(new Reservation(1,"0610-JS", new ArrayList<>(List.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7"))))));
        List<Reservation> expectedReservations = new ArrayList<Reservation>(List.of(reservation1));
        List<Reservation> actualReservations = reservationService.reservationSearch("Name1 Surname1");
        assertEquals(expectedReservations, actualReservations);}

    @Test
    public void testAddReservation() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1)));
        ReservationService reservationService1 = new ReservationService(collectionReservationDao1);
        reservationService1.addReservation(reservation2);
        assertEquals(2, collectionReservationDao1.getAllReservations().size());}
    @Test
    public void testDeleteReservation() {
        CollectionReservationDao collectionReservationDao1 = new CollectionReservationDao(new ArrayList<>(List.of(reservation1, reservation2)));
        ReservationService reservationService1 = new ReservationService(collectionReservationDao1);
        reservationService1.deleteReservation(1);
        assertEquals(1, collectionReservationDao1.getAllReservations().size());}

}
