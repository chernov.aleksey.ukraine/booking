package test;
import flight.CollectionFlightDao;
import flight.Flight;
import flight.FlightService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {
    @Mock
    private CollectionFlightDao collectionFlightDao;
    @InjectMocks
    private FlightService flightService;
    Flight flight01 = new Flight("0012-VN", "00:12", "Vienna", 7);
    Flight flight02 = new Flight("0115-BS", "01:15", "Brussels", 5);
    @Test
    public void testGetAllFlights() {
        when(collectionFlightDao.getAllFlights()).thenReturn(new ArrayList<>(List.of(new Flight("0012-VN", "00:12", "Vienna", 7))));
        List<Flight> actualFlights = flightService.getAllFlights();
        assertEquals(1, actualFlights.size());}
    @Test
    public void testFlightInfo() {
        when(collectionFlightDao.getAllFlights()).thenReturn(new ArrayList<>(List.of(new Flight("0012-VN", "00:12", "Vienna", 7))));
        Flight actualFlight = flightService.flightInfo("0012-VN");
        assertEquals(flight01, actualFlight);}
    @Test
    public void testFlightSearch() {
        when(collectionFlightDao.getAllFlights()).thenReturn(new ArrayList<>(List.of(flight01, flight02 )));
        List<Flight> expectedFlights = new ArrayList<Flight>(List.of(flight01));
        List<Flight> actualFlights = flightService.flightSearch( "Vienna", 7, LocalDate.now().plusDays(1).toString());
        assertEquals(expectedFlights, actualFlights);}
}
