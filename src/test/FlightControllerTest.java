package test;
import flight.CollectionFlightDao;
import flight.FlightController;
import flight.Flight;
import flight.FlightService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)

public class FlightControllerTest {

    @Mock
    private FlightService flightService;
    @InjectMocks
    private FlightController flightController;
    Flight flight01 = new Flight("0012-VN", "00:12", "Vienna", 7);
    Flight flight02 = new Flight("0115-BS", "01:15", "Brussels", 5);

    @Test
    public void testGetAllFlights() {
        when(flightService.getAllFlights()).thenReturn(new ArrayList<>(List.of(new Flight("0012-VN", "00:12", "Vienna", 7))));
        List<Flight> actualFlights = flightController.getAllFlights();
        assertEquals(1, actualFlights.size());}
    @Test
    public void testFlightData() {
        CollectionFlightDao collectionFlightDao1 = new CollectionFlightDao(new ArrayList<>(List.of(flight01, flight02)));
        FlightService flightService1 = new FlightService(collectionFlightDao1);
        FlightController flightController1  = new FlightController(flightService1);
        Flight actualFlight = flightController1.flightData("0012-VN");
        assertEquals(flight01, actualFlight);}
    @Test
    public void testFlightSearch() {
        CollectionFlightDao collectionFlightDao1 = new CollectionFlightDao(new ArrayList<>(List.of(flight01, flight02)));
        FlightService flightService1 = new FlightService(collectionFlightDao1);
        FlightController flightController1  = new FlightController(flightService1);
        List<Flight> expectedFlights = new ArrayList<Flight>(List.of(flight01));
        List<Flight> actualFlights = flightController1.flightSearch( "Vienna", 7, LocalDate.now().plusDays(1).toString());
        assertEquals(expectedFlights, actualFlights);}

}
