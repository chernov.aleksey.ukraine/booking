package test;
import flight.CollectionFlightDao;
import flight.Flight;
import flight.FlightService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)

public class CollectionFlightDaoTest {
    @Mock
    private ArrayList<Flight> flightList;
    @InjectMocks
    private CollectionFlightDao collectionFlightDao;
    Flight flight01 = new Flight("0012-VN", "00:12", "Vienna", 7);
    Flight flight02 = new Flight("0115-BS", "01:15", "Brussels", 5);
    @Test
    public void testGetAllFlights() {
        CollectionFlightDao collectionFlightDao1 = new CollectionFlightDao(new ArrayList<>(List.of(flight01, flight02)));
        List<Flight> actualFlights = collectionFlightDao1.getAllFlights();
        assertEquals(2, actualFlights.size());}
    @Test
    public void testLoadDownloadData() {
        CollectionFlightDao collectionFlightDao1 = new CollectionFlightDao(new ArrayList<>(List.of(flight01, flight02)));
        List<Flight> control = collectionFlightDao1.getAllFlights();
        File file = new File("test_flight_data");
        collectionFlightDao1.loadData((ArrayList<Flight>) control, "test_flight_data");
        collectionFlightDao1.setFlightList(new ArrayList<>());
        collectionFlightDao1.downloadData("test_flight_data");
        file.delete();
        assertEquals(control, collectionFlightDao1.getAllFlights() );}

}
