package flight;
import java.util.ArrayList;
public interface FlightDao {
    public ArrayList<Flight> getAllFlights();
    public void loadData(ArrayList<Flight> flights, String name);
    public void downloadData(String name);
}
