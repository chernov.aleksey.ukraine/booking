package flight;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Objects;

import static java.lang.Integer.parseInt;

public class Flight implements java.io.Serializable{
    private String flightId;
    private String departureTime;
    private String destination;
    private int freePlaces;


    public String getFlightId() {return flightId;}
    public void setFlightId(String flightId) {this.flightId = flightId;}
    public String getDepartureTime() {return departureTime;}
    public void setDepartureTime(String departureTime) {this.departureTime = departureTime;}
    public String getDestination() {return destination;}
    public void setDestination(String destination) {this.destination = destination;}
    public int getFreePlaces() {return freePlaces;}
    public void setFreePlaces(int freePlaces) {this.freePlaces = freePlaces;}

    public Flight(){};
    public Flight(String flightId, String departureTime, String destination, int freePlaces){
        this.flightId = flightId;
        this.departureTime = departureTime;
        this.destination = destination;
        this.freePlaces = freePlaces;

    };

    public  String departureDate (){
        String [] departureTimeArray = this.getDepartureTime().split(":");
        String [] nowString  = LocalTime.now().toString().split(":");
        if (parseInt(nowString[0]+nowString[1]) < parseInt(departureTimeArray[0]+departureTimeArray[1])) {
            return LocalDate.now().toString();} else { return LocalDate.now().plusDays(1).toString();}}

    public void changeFreePlaces(int change){
        this.setFreePlaces(this.getFreePlaces() + change);};

    @Override
    public String toString() {
        int length = 18 - this.getDestination().length();
        return this.departureDate() + "      " + this.getFlightId() +"        " + this.getDepartureTime() +"        "  + this.getDestination() + String.format("%" + length + "." + length + "s", "")  +this.getFreePlaces();}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return flightId.equals(flight.flightId) && departureTime.equals(flight.departureTime) && freePlaces == flight.freePlaces;    }

    @Override
    public int hashCode() {
        return Objects.hash(flightId, departureTime, destination, freePlaces);
    }
}
