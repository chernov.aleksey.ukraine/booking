package flight;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import static java.lang.Integer.parseInt;
public class FlightService {
    CollectionFlightDao collectionFlightDao;
    public FlightService(CollectionFlightDao collectionFlightDao) {
        this.collectionFlightDao = collectionFlightDao;
    }
    public ArrayList<Flight> getAllFlights() {return collectionFlightDao.getAllFlights();}
    public Flight flightInfo(String flightId) {
        try {return collectionFlightDao.getAllFlights().stream().filter(entry -> entry.getFlightId().equals(flightId)).findFirst().get();
        } catch (NoSuchElementException e) {
            System.out.println(">>> >>> THERE IS NO FLIGHT WITH THIS FLIGHT-ID <<< <<<");
            return null;}};
    public List<Flight> flightSearch( String destination, int places, String date) {
        return collectionFlightDao.getAllFlights().stream().filter(entry ->
                entry.getDestination().equals(destination)
                && entry.getFreePlaces() >= places
                && entry.departureDate().equals(date))
                .collect(Collectors.toList());}
    public void loadData(ArrayList<Flight> flights, String name) {this.collectionFlightDao.loadData( flights, name);}
    public void downloadData(String name) {this.collectionFlightDao.downloadData(name);}
}
