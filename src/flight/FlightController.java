package flight;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.lang.Integer.parseInt;

public class FlightController {
    FlightService flightService;
    public FlightController(FlightService flightService) { this.flightService = flightService;}
    public ArrayList<Flight> getAllFlights() {return flightService.getAllFlights();}


    public Flight flightData (String flightId){
        try {
            return this.flightService.flightInfo(flightId);
        } catch (NullPointerException e) {
            System.out.println(">>>  >>>  THERE IS NO DATA - YOU SHOULD RETRY <<<  <<<");
            return null;}};

    public List<Flight> flightSearch(String destination, int places, String date){
        return this.flightService.flightSearch( destination, places, date);
    }
    public void loadData(ArrayList<Flight> flights, String name) {this.flightService.loadData( flights,  name); }


    public void downloadData(String name) {this.flightService.downloadData( name); }

}
