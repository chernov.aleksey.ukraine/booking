package flight;

import java.io.*;
import java.util.ArrayList;

public class CollectionFlightDao implements FlightDao {
    private ArrayList<Flight> flightList;
    public CollectionFlightDao(ArrayList<Flight> flightList) {
        this.flightList = flightList;
    }
    public void setFlightList(ArrayList<Flight> flightList) {this.flightList = flightList;}
    @Override
    public ArrayList<Flight> getAllFlights(){return this.flightList;}
    @Override
    public void loadData(ArrayList<Flight> flights, String name) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(name))) {
            objectOutputStream.writeObject(flights);
        } catch (IOException e) {throw new RuntimeException(e);}}
    @Override
    public void downloadData( String name) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(name))) {
            ArrayList<Flight> flights = (ArrayList<Flight>) objectInputStream.readObject();
            this.setFlightList(flights);
        } catch (IOException e) {System.out.println("Не удолось найти сохраненные данные");
        }catch (ClassNotFoundException e){throw new RuntimeException(e);}}
}
//"flight_data"