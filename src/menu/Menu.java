package menu;
import flight.CollectionFlightDao;
import flight.Flight;
import flight.FlightController;
import flight.FlightService;
import org.w3c.dom.ls.LSOutput;
import reservation.CollectionReservationDao;
import reservation.Reservation;
import reservation.ReservationController;
import reservation.ReservationService;

import java.time.LocalTime;
import java.util.*;

import static java.lang.Integer.parseInt;

public class Menu {
    static Scanner scanner = new Scanner(System.in);
    public  static int integerInput (String theQuest){System.out.println(theQuest);
        while(true) {System.out.print("Your option is: ");
        if (!scanner.hasNextInt()) {System.out.println("You should enter the option with an integer");
            scanner.next();
            continue;
        } else {return scanner.nextInt();}}}
    public static String lineInput (String theQuest){System.out.println(theQuest);
        String string ="";
        System.out.print("Your option is: ");
        while(string.length() == 0) {string =scanner.next();}
        return string;}
    public static String personString(){return lineInput("Введіть ім'я").trim() + " " + lineInput("Введіть прізвище").trim();}
    public static void flightSchedule(ArrayList<Flight> array) {System.out.println(">>> >>> >>>              FLIGHTS SCHEDULE                <<< <<< <<<");
        System.out.println("---------------------------------------------------------------------");
        System.out.println("Departure Date   Flight   Departure time   Destination   Free Places");
        System.out.println("---------------------------------------------------------------------");
        System.out.println(">>> >>> >>>                   TODAY                      <<< <<< <<<");
        System.out.println("---------------------------------------------------------------------");
        array.forEach(e -> {String [] departureTimeArray = e.getDepartureTime().split(":");
            String [] nowString  = LocalTime.now().toString().split(":");
            if (parseInt(nowString[0]+nowString[1]) < parseInt(departureTimeArray[0]+departureTimeArray[1])) {System.out.println(e);}});
        System.out.println("---------------------------------------------------------------------");
        System.out.println(">>> >>> >>>                  TOMORROW                     <<< <<< <<<");
        System.out.println("---------------------------------------------------------------------");
        array.forEach(e -> {String [] departureTimeArray = e.getDepartureTime().split(":");
            String [] nowString  = LocalTime.now().toString().split(":");
            if (parseInt(nowString[0]+nowString[1]) > parseInt(departureTimeArray[0]+departureTimeArray[1])) {System.out.println(e);}});}
    public static void flightInfo (Flight flightToShow){try{int length = 13 - flightToShow.getDestination().length();
            System.out.println("Flight: " + flightToShow.getFlightId());
            System.out.println("..........................................................................................");
            System.out.println("Departure Date: " + flightToShow.departureDate() + "       Departure time: " + flightToShow.getDepartureTime() );
            System.out.println("Destination: " + flightToShow.getDestination() +String.format("%" + length + "." + length + "s", "")+ "       Free places: " + flightToShow.getFreePlaces());
            System.out.println("..........................................................................................");}
        catch (NullPointerException e) {System.out.println(">>>  >>>  THERE IS NO DATA - YOU SHOULD RETRY <<<  <<<");}};
    public static void menu(FlightController flightController, ReservationController reservationController) {
        System.out.println("\nGreetings, to continue, choose the option from the list below: ");
        String menu = """
                 \n- 1. Онайн-табло.
                - 2. Подивитися інформацію про рейс.
                - 3. Пошук та бронювання рейсу.
                - 4. Скасувати бронювання.
                - 5. Мої рейси.
                - 6. Вихід.
                 """;
        while (true){
            try {
                System.out.println(menu);
                switch (integerInput("")) {
                    case 1:flightSchedule(flightController.getAllFlights());
                        break;
                    case 2:flightInfo(flightController.flightData(lineInput ("Введіть ІD рейсу")));
                        break;
                    case 3:
                        int ticketsNumber=integerInput("Введіть кількість порібних квітків");
                        ArrayList<Flight> appropriateFlights = (ArrayList<Flight>) flightController.flightSearch(
                            lineInput ("Введіть місце призначення"), ticketsNumber,
                            "2023-08-"+lineInput ("Введіть дату в форматі ДД. Сьогоді, чи завтра"));
                        if (appropriateFlights.size() == 0) {System.out.println("Увага! Немає рейсів, які відповідають Вашим умовам!");
                            break;}
                        System.out.println("Item number  Departure Date   Flight   Departure time   Destination   Free Places");
                        for (int i = 0; i< appropriateFlights.size(); i++){System.out.print(i+1  + "             ");
                            System.out.println(appropriateFlights.get(i));}
                        int itemNumber = -1;
                        while (itemNumber == -1){itemNumber = integerInput("Введіть Item number бажаного рейсу або 0, щоб повернутися в головне меню.");
                        if (itemNumber > appropriateFlights.size() || itemNumber < 0 ) {System.out.println("Увага! Введено помилковий Item number!");
                            itemNumber = -1;}}
                        if (itemNumber == 0 ){break;}
                        System.out.println("Введіть інформацію про пассажирів");
                        ArrayList<String> persons = new ArrayList<>();
                        for (int i = 0; i<ticketsNumber; i++ ){ int passengerNumber = i + 1;
                        persons.add(lineInput("Введіть Ім'я пассажира " + passengerNumber).trim() +" " + lineInput("Введіть Прізвище пассажира " + passengerNumber).trim());}
                        reservationController.addReservation(new Reservation(reservationController.getAllReservations().get(reservationController.getAllReservations().size()-1).getReservationId()+1,
                            appropriateFlights.get(itemNumber-1).getFlightId(), persons));
                        flightController.flightData(appropriateFlights.get(itemNumber-1).getFlightId()).changeFreePlaces(-ticketsNumber);
                        flightController.loadData(flightController.getAllFlights(), "flight_data");
                        reservationController.loadData(reservationController.getAllReservations(), "reservation_data");
                        break;
                    case 4:
                        int deletedReservationID = integerInput ("Введіть ID бронювання, яке має бути скасованим");
                        flightController.flightData(reservationController.findReservation(deletedReservationID).getFlightId())
                                .changeFreePlaces(reservationController.findReservation(deletedReservationID).getPersons().size());
                        reservationController.deleteReservation(deletedReservationID);
                        flightController.loadData(flightController.getAllFlights(), "flight_data");
                        reservationController.loadData(reservationController.getAllReservations(), "reservation_data");
                        break;
                    case 5:
                       List<Reservation> arrayOfPersonReservation =  reservationController.reservationSearch(personString());
                        arrayOfPersonReservation.forEach(entry ->{
                            System.out.println("__________________________________________________________________________________________");
                            System.out.println("RESERVATION. ID: " + entry.getReservationId());
                            flightInfo(flightController.flightData(entry.getFlightId()));
                            System.out.println("Persons who were registered to this flight with this registration transaction: ");
                            System.out.println(entry.getPersons());
                            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");});
                        if (arrayOfPersonReservation.size() == 0){System.out.println("Sorry, there are no flights for this person.");};
                        break;
                    case 6:
                        flightController.loadData(flightController.getAllFlights(),"flight_data");
                        reservationController.loadData(reservationController.getAllReservations(), "reservation_data");
                        System.out.println("EXIT");
                        System.exit(0);
                    default:
                        System.out.println("There is NO such an option");
                        break;}} catch (InputMismatchException e){System.out.println(">>> >>> >>> Некоректні данні! Спробуйте ще раз. <<< <<< <<<");}}}
    }

