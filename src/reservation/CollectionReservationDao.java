package reservation;

import flight.Flight;

import java.io.*;
import java.util.ArrayList;

public class CollectionReservationDao implements ReservationDao{
    private ArrayList<Reservation> reservationList;
    public CollectionReservationDao(ArrayList<Reservation> reservationList) {
        this.reservationList = reservationList;
    }
    public void setReservationList(ArrayList<Reservation> reservationList) {this.reservationList = reservationList;}
    @Override
    public ArrayList<Reservation> getAllReservations() {return this.reservationList;}
    @Override
    public void addReservation(Reservation newReservation){this.reservationList.add(newReservation);}
    @Override
    public void deleteReservation (int index) { this.reservationList.remove(index); }
    @Override
    public void loadData(ArrayList<Reservation> reservations, String name) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(name))) {
            objectOutputStream.writeObject(reservations);
        } catch (IOException e) {throw new RuntimeException(e);}}
    @Override
    public void downloadData(String name) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(name))) {
            ArrayList<Reservation> reservations = (ArrayList<Reservation>) objectInputStream.readObject();
            this.setReservationList(reservations);
        } catch (IOException e) {System.out.println("Не удолось найти сохраненные данные");}
        catch (ClassNotFoundException e) {throw new RuntimeException(e);}}
}
//reservation_data