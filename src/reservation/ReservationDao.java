package reservation;

import flight.Flight;

import java.util.ArrayList;

public interface ReservationDao {
    public ArrayList<Reservation> getAllReservations();
    public void addReservation(Reservation newReservation);
    public void deleteReservation(int index);
    public void loadData(ArrayList<Reservation> reservations, String Name);
    public void downloadData(String name);
}
