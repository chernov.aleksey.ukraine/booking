package reservation;



import java.util.ArrayList;
import java.util.Objects;

public class Reservation implements java.io.Serializable{
    private int reservationId;
    private String flightId;
    private ArrayList<String> persons = new ArrayList<>();

    public int getReservationId() {return reservationId;}
    public void setReservationId(int number) {this.reservationId = reservationId;}
    public String getFlightId() {return flightId;}
    public void setFlightId(String flightId) {this.flightId = flightId;}
    public ArrayList<String> getPersons() {return persons;}
    public void setPersons(ArrayList<String> persons) {this.persons = persons;}

    public Reservation(){}
    public Reservation(int reservationId, String flightId, ArrayList<String> persons){
        this.reservationId = reservationId;
        this.flightId = flightId;
        this.persons = persons;}


    @Override
    public String toString() {
        return this.getReservationId() + "   " + this.getFlightId() + "   " + this.getPersons();}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation reservation = (Reservation) o;
        return reservationId == reservation.reservationId && flightId.equals(reservation.flightId) && Objects.equals(persons, reservation.persons);}

    @Override
    public int hashCode() {
        return Objects.hash(flightId, reservationId, persons);
    }

}
