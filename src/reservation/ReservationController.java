package reservation;

import flight.FlightController;
import flight.FlightService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ReservationController {
    ReservationService reservationService;
    public ReservationController(ReservationService reservationService) {this.reservationService = reservationService;}
    public ArrayList<Reservation> getAllReservations() {return reservationService.getAllReservations();}
    public Reservation findReservation(int reservationId) {
        try{return this.reservationService.getAllReservations().stream().filter(entry -> entry.getReservationId() == reservationId).findFirst().get();}
        catch (NoSuchElementException err){System.out.println("There is no reservation with '" + reservationId + "' reservation ID. Try again!");
        return null;}}
    public void addReservation(Reservation newReservation){this.reservationService.addReservation(newReservation);}
    public void deleteReservation(int index) {try{int number = this.reservationService.getAllReservations().
        indexOf(this.reservationService.getAllReservations().stream().filter(entry -> entry.getReservationId() == index).findFirst().get());
        this.reservationService.deleteReservation(number);} catch (NoSuchElementException err){
        System.out.println("There is no reservation with '" + index + "' reservation ID. Try again!");}}
    public ArrayList<Reservation> reservationSearch(String nameSurname){
        return (ArrayList<Reservation>)this.reservationService.reservationSearch(nameSurname);
    }
    public void loadData(ArrayList<Reservation> reservations, String name) {this.reservationService.loadData(reservations, name);}
    public void downloadData(String name) {this.reservationService.downloadData(name);}
}
