package reservation;

import flight.CollectionFlightDao;
import flight.Flight;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReservationService {
    CollectionReservationDao collectionReservationDao;

    public ReservationService(CollectionReservationDao collectionReservationDao) {this.collectionReservationDao = collectionReservationDao;}
    public ArrayList<Reservation> getAllReservations() {return collectionReservationDao.getAllReservations();}
    public void addReservation(Reservation newReservation){this.collectionReservationDao.addReservation(newReservation);}
    public void deleteReservation (int index) { this.collectionReservationDao.deleteReservation(index); }
    public List<Reservation> reservationSearch(String nameSurname) {return collectionReservationDao.getAllReservations()
            .stream().filter(entry->entry.getPersons().contains(nameSurname)).collect(Collectors.toList());}
    public void loadData(ArrayList<Reservation> reservations, String name) {
        this.collectionReservationDao.loadData(reservations, name);
    }
    public void downloadData(String name) {
        this.collectionReservationDao.downloadData(name);
    }
}
