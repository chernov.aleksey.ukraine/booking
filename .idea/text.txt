
//        Flight flight01 = new Flight("0012-VN", "00:12", "Vienna", 7);
//        Flight flight02 = new Flight("0115-BS", "01:15", "Brussels", 5);
//        Flight flight03 = new Flight("0232-SJ", "02:32", "Sarajevo", 3);
//        Flight flight04 = new Flight("0314-WT", "03:14", "Washington", 6);
//        Flight flight05 = new Flight("0506-NY", "05:06", "New-York", 2);
//        Flight flight06 = new Flight("0520-PG", "05:20", "Podgorica", 3);
//        Flight flight07 = new Flight("0545-WS", "05:45", "Warsaw", 4);
//        Flight flight08 = new Flight("0600-BL", "06:00", "Bratislava", 6);
//        Flight flight09 = new Flight("0610-JS", "06:10", "Jerusalem", 2);
//        Flight flight10 = new Flight("0612-WL", "06:12", "Wellington", 8);
//        Flight flight11 = new Flight("0630-BR", "06:30", "Berlin", 6);
//        Flight flight12 = new Flight("0645-LX", "06:45", "Luxembourg", 3);
//        Flight flight13 = new Flight("0710-AK", "07:10", "Ankara", 4);
//        Flight flight14 = new Flight("0733-LN", "07:33", "London", 2);
//        Flight flight15 = new Flight("0735-WS", "07:35", "Warsaw", 5);
//        Flight flight16 = new Flight("0740-DB", "07:40", "Dubai", 4);
//        Flight flight17 = new Flight("0755-LB", "07:55", "Lisbon", 8);
//        Flight flight18 = new Flight("0900-SO", "09:00", "Seoul", 4);
//        Flight flight19 = new Flight("1007-PR", "10:07", "Paris", 5);
//        Flight flight20 = new Flight("1140-RA", "11:40", "Riyadh", 3);
//        Flight flight21 = new Flight("1320-AT", "13:20", "Athens", 2);
//        Flight flight22 = new Flight("1430-NY", "14:30", "New-York", 4);
//        Flight flight23 = new Flight("1650-BH", "16:50", "Bucharest", 3);
//        Flight flight24 = new Flight("1700-BR", "17:00", "Berlin", 2);
//        Flight flight25 = new Flight("1720-SJ", "17:20", "Skopje", 4);
//        Flight flight26 = new Flight("1744-BR", "17:44", "Berlin", 2);
//        Flight flight27 = new Flight("1830-LN", "18:30", "London", 3);
//        Flight flight28 = new Flight("1846-PR", "18:46", "Paris", 2);
//        Flight flight29 = new Flight("1910-TY", "19:10", "Tokyo", 7);
//        Flight flight30 = new Flight("2015-BP", "20:15", "Budapest", 4);

//        flights.add(flight01); flights.add(flight02); flights.add(flight03); flights.add(flight04); flights.add(flight05);
//        flights.add(flight06); flights.add(flight07); flights.add(flight08); flights.add(flight09); flights.add(flight10);
//        flights.add(flight11); flights.add(flight12); flights.add(flight13); flights.add(flight14); flights.add(flight15);
//        flights.add(flight16); flights.add(flight17); flights.add(flight18); flights.add(flight19); flights.add(flight20);
//        flights.add(flight21); flights.add(flight22); flights.add(flight23); flights.add(flight24); flights.add(flight25);
//        flights.add(flight26); flights.add(flight27); flights.add(flight28); flights.add(flight29); flights.add(flight30);


//        ArrayList<String> persons01 = new ArrayList<>(Set.of("Name1 Surname1", "Name2 Surname2", "Name7 Surname7"));
//        ArrayList<String> persons02 = new ArrayList<>(Set.of("Name3 Surname3", "Name4 Surname4"));
//        ArrayList<String> persons03 = new ArrayList<>(Set.of("Name5 Surname5"));
//        ArrayList<String> persons04 = new ArrayList<>(Set.of("Name8 Surname8", "Name7 Surname7"));
//        ArrayList<String> persons05 = new ArrayList<>(Set.of("Name9 Surname9", "Name10 Surname10"));
//        ArrayList<String> persons06 = new ArrayList<>(Set.of("Name10 Surname10", "Name7 Surname7", "Name12 Surname12"));
//        ArrayList<String> persons07 = new ArrayList<>(Set.of("Name11 Surname11", "Name14 Surname14"));
//        ArrayList<String> persons08 = new ArrayList<>(Set.of("Name13 Surname13", "Name5 Surname5"));
//        ArrayList<String> persons09 = new ArrayList<>(Set.of("Name1 Surname1"));
//        ArrayList<String> persons10 = new ArrayList<>(Set.of("Name9 Surname9", "Name11 Surname11", "Name6 Surname6"));
//
//        Reservation reservation1 = new Reservation(1,"0610-JS", persons01 );
//        Reservation reservation2 = new Reservation(2,"0506-NY", persons02 );
//        Reservation reservation3 = new Reservation(3,"0645-LX", persons03 );
//        Reservation reservation4 = new Reservation(4,"0733-LN", persons04 );
//        Reservation reservation5 = new Reservation(5,"1320-AT", persons05 );
//        Reservation reservation6 = new Reservation(6,"1700-BR", persons06 );
//        Reservation reservation7 = new Reservation(7,"1846-PR", persons07 );
//        Reservation reservation8 = new Reservation(8,"0506-NY", persons08 );
//        Reservation reservation9 = new Reservation(9,"0733-LN", persons09 );
//        Reservation reservation10 = new Reservation(10,"1320-AT", persons10 );


//        reservations.add(reservation1); reservations.add(reservation2); reservations.add(reservation3); reservations.add(reservation4); reservations.add(reservation5);
//        reservations.add(reservation6); reservations.add(reservation7); reservations.add(reservation8); reservations.add(reservation9); reservations.add(reservation10);
